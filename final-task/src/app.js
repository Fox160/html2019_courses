/* src/app.js */

// Styles
import 'styles/_app.scss';

$(document).ready(() => {
  require('scripts/header');
  require('scripts/mobile-menu');
  require('scripts/slider');
  require('scripts/scroll-top');
});
