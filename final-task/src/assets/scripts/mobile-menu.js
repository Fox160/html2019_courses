//Burger-button
$('.navbar-responsive').click(function() {
  $('.menu-navbar').slideToggle();
  $('.dropdown-content').css('display', 'none');
  $('body').toggleClass('body-hide');
});

$('.dropbtn').click(function() {
  if ($('.dropdown-content:visible').length) $('.dropdown-content').hide(400);
  else $('.dropdown-content').show(400);
});
